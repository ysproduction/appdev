<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    public function run()
    {
        $date = Carbon::now();
        $createdDate = clone($date);

            DB::table('admins')->insert(
                ['access_level' => "Admin",
                'email' => "ysproduction@gmail.com",
                'password' => Hash::make("admin123"),
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
    }
}
