<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class PackageSeeder extends Seeder
{
    public function run()
    {
        $date = Carbon::now();
        $createdDate = clone($date);

        DB::table('packages')->insert([
            ['description' => "Photography",
            'service_id' => "1",
            'price' => "5000",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['description' => "Videography",
            'service_id' => "2",
            'price' => "10000",
            'created_at' => $createdDate,
            'updated_at' => $createdDate],

            ['description' => "Cinematography",
            'service_id' => "3",
            'price' => "15000",
            'created_at' => $createdDate,
            'updated_at' => $createdDate]
        ]);
    }
}

