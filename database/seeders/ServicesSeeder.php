<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class ServicesSeeder extends Seeder
{
    public function run()
    {
        $services = [
            "Photography",
            "Videography",
            "Cinematography",
            "Assorted"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($services as $service) {
            DB::table('services')->insert(
                ['service_description' => $service,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
