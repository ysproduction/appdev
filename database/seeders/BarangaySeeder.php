<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class BarangaySeeder extends Seeder
{
    public function run()
    {
        $barangays = [
            "Barangay 1",
            "Barangay 2"
        ];

        $date = Carbon::now();
        $createdDate = clone($date);

        foreach($barangays as $barangay) {
            DB::table('barangays')->insert(
                ['barangay_description' => $barangay,
                'created_at' => $createdDate,
                'updated_at' => $createdDate]
            );
        }
    }
}
