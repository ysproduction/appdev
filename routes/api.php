<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\BarangaysController;
use App\Http\Controllers\API\ClientsController;
use App\Http\Controllers\API\HandlersController;
use App\Http\Controllers\API\OrdersController;
use App\Http\Controllers\API\PackagesController;
use App\Http\Controllers\API\ServicesController;
use App\Http\Controllers\APi\ClientAuthController;
use App\Http\Controllers\APi\AdminAuthController;

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    
    Route::group(['prefix' => 'clients'], function () {
        Route::post('/login', [ClientAuthController::class, 'login']);
        Route::post('/register', [ClientAuthController::class, 'register']);
        Route::post('/logout', [ClientAuthController::class, 'logout']);
        Route::post('/refresh', [ClientAuthController::class, 'refresh']);
        Route::get('/me', [ClientAuthController::class, 'me']);  
    });

    Route::group(['prefix' => 'admin'], function () {
        Route::post('/login', [AdminAuthController::class, 'login']);
        Route::post('/logout', [AdminAuthController::class, 'logout']);
        Route::post('/refresh', [AdminAuthController::class, 'refresh']);
        Route::get('/me', [AdminAuthController::class, 'me']);  
    });
});


Route::apiResource('/barangays', BarangaysController::class);
Route::apiResource('/clients', ClientsController::class);
Route::apiResource('/handlers', HandlersController::class);
Route::apiResource('/services', ServicesController::class);
Route::apiResource('/packages', PackagesController::class);
Route::apiResource('/orders', OrdersController::class);
