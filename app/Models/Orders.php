<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    use HasFactory;

    protected $guarded = []; 

    public function clientRelation(){
        return $this->hasOne(Clients::class, 'client_id', 'client_id');
    }

    public function packageRelation(){
        return $this->hasOne(Packages::class, 'package_id', 'package_id');
    }
}
