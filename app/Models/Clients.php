<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;

class Clients extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    protected $guarded = [];

    public function barangayRelation(){
        return $this->hasOne(Barangays::class, 'barangay_id', 'barangay_id');
    }

    public function ordersRelation(){
        return $this->hasMany(Orders::class, 'client_id', 'client_id');
    }

    protected $fillable = [
        'access_level',
        'barangay_id',
        'first_name',
        'middle_name',
        'last_name',
        'contact_num',
        'email',
        'password'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}