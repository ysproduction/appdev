<?php

namespace App\Http\Controllers\API;

use App\Models\Clients;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class ClientAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:client', ['except' => ['login', 'register']]);
        
    }

    public function login()
    {
        $credentials = request(['email', 'password', 'access_level']);

        if (! $token = auth('client')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'barangay_id' => 'required|exists:barangays,barangay_id',
            'access_level' => 'required',
            'first_name' =>  'required',
            'middle_name' =>  'required',
            'last_name' =>  'required',
            'contact_num' =>  'required',
            'email' =>  'required|email|unique:clients,email',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = Clients::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user,
            'key' => 'value'
        ], 201);
    }

    public function me()
    {
        return response()->json(auth('client')->user());
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth('client')->refresh());
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('client')->factory()->getTTL() * 60
        ]);
    }
}
