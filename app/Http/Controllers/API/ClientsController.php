<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ClientsRequest;
use App\Http\Controllers\Controller;
use App\Models\Clients;

class ClientsController extends Controller
{
    public function index()
    {
        $clients = DB::table('clients')
        ->join('barangays', 'barangays.barangay_id', '=', 'clients.barangay_id')
        ->select('clients.*', 'barangays.*')->get();

        return response()->json($clients, 200);
    }
   
    public function store(ClientsRequest $request)
    {
        $clients = Clients::create($request->all());
        return response()->json($clients, 201);
    }

    public function update(ClientsRequest $request, $id)
    {
        $clients = Clients::where('clients_id', $id)->update($request->all(), $id);
        return response()->json($clients, 200);
    }
}