<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\BarangaysRequest;
use App\Http\Controllers\Controller;
use App\Models\Barangays;

class BarangaysController extends Controller
{
    public function index()
    {
        $barangays = Barangays::all();

        return response()->json($barangays, 200);
    }
   
    public function store(BarangaysRequest $request)
    {
        $barangays = Barangays::create($request->all());
        return response()->json($barangays, 201);
    }
}
