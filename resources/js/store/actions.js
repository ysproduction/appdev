import axios from "../axiosConfig.js";

export const fetchBarangay = ({ commit }) => {
    axios.get('/barangays')
        .then(response => {
            commit('setBarangay', response.data);
        })
}

export const fetchClients = ({ commit }) => {
    axios.get('/clients')
        .then(response => {
            commit('setClients', response.data);
        })
}

export const fetchServices = ({ commit }) => {
    axios.get('/services')
        .then(response => {
            commit('setServices', response.data);
        })
}

export const fetchPackages = ({ commit }) => {
    axios.get('/packages')
        .then(response => {
            commit('setPackages', response.data);
        })
}

export const fetchOrders = ({ commit }) => {
    axios.get('/orders')
        .then(response => {
            commit('setOrders', response.data);
        })
}
export const createClient = async ({ commit }, data) => {
    const res = await axios.post('/auth/clients/register', data)
        .then(response => {
            commit('createClients', eval(response.data));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const createPackage = async ({ commit }, data) => {
    const res = await axios.post('/packages', data)
        .then(response => {
            commit('createPackages', eval(response.data));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const updatePackage = async ({ commit }, data) => {
    const res = await axios.put(`/packages/${data.package_id}`, data)
        .then(response => {
            commit('updatePackages', eval(response.data));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const updateStatuss = async ({ commit }, data) => {
    const res = await axios.put(`/orders/${data.order_id}`, data)
        .then(response => {
            commit('updateStatuses', eval(response.data));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const createSchedules = async ({ commit }, data) => {
    const res = await axios.post('/orders', data)
        .then(response => {
            commit('createSchedules', eval(response.data));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const loginUser = async ({ commit }, user) => {
    const res = await axios.post('/auth/clients/login', user)
        .then(response => {
            commit('setClients', response.data.user);
            commit('setToken', response.data.token);
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const loginAdmin = async ({ commit }, user) => {
    const res = await axios.post('/auth/admin/login', user)
        .then(response => {
            commit('setAdmin', response.data.user);
            commit('setToken', response.data.token);
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const check_clientUser = async ({ commit }) => {
    const res = await axios.get('/auth/clients/me?token=', + localStorage.getItem("token"))
        .then(response => {
            commit('setUsers', response.data);
            commit('setToken', localStorage.getItem("token"));
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const logoutUser = async ({ commit }) => {
    const res = await axios.post('/auth/clients/logout?')
        .then(response => {
            commit("unsetUser");
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}

export const logoutAdmin = async ({ commit }) => {
    const res = await axios.post('/auth/admin/logout?')
        .then(response => {
            commit("unsetUser");
            return response;
        }).catch(err => {
            return err.response;
        });
    return res;
}