export const getBarangay = (state =>{
    return state.barangay;
})

export const getClients = (state =>{
    return state.clients;
})

export const getServices = (state =>{
    return state.services;
})

export const getPackages = (state =>{
    return state.packages;
})

export const getOrders = (state =>{
    return state.orders;
})