import axios from "../axiosConfig.js";

export const setComponentKey = (state, number) => {
  state.componentKey = number;
}

export const setBarangay = (state, data) => {
  state.barangay = data;
}

export const createClients = (state, data) => {
  state.clients.push(data);
}

export const createPackages = (state, data) => {
  state.packages.push(data);
}

export const updatePackages = (state, data) =>{
  const index = state.packages.findIndex((item) => item.id === data.id);
    if (index !== -1) state.packages.splice(index, 1, { ...data });
}

export const updateStatuses = (state, data) =>{
  const index = state.orders.findIndex((item) => item.id === data.id);
    if (index !== -1) state.orders.splice(index, 1, { ...data });
}


export const createSchedules = (state, data) => {
  state.orders.push(data);
}

export const setClients = (state, client) => {
  state.clients = client;
}

export const setPackages = (state, packages) => {
  state.packages = packages;
}

export const setAdmin = (state, admin) => {
  state.admins = admin;
}

export const setServices = (state, data) => {
  state.services = data;
}

export const setOrders = (state, data) => {
  state.orders = data;
}

export const setClientData = (state, data) =>{
  state.clientData = data
}

export const setToken = (state, token) => {
  localStorage.setItem("token", token);
  state.token = token;
  const bearer_token = localStorage.getItem("token") || "";
  axios.defaults.headers.common["Authorization"] = `Bearer ${bearer_token}`;
}

export const unsetUser = (state) => {
  localStorage.removeItem("token");
  state.token = "";
  axios.defaults.headers.common["Authorization"] = "";
}
export const setUsers = (state, user) => {
  state.users = user;
}