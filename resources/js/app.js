import Vue from 'vue'

import App from './App.vue'
import Toast from "vue-toastification";

import navbar from './components/Navbar.vue'
Vue.component('navbar', navbar)

import sidenav from './components/sidenav.vue'
Vue.component('sidenav', sidenav)

import topnav from './components/topnav.vue'
Vue.component('topnav', topnav)

import nav from './components/RequestNavbar.vue'
Vue.component('reqNav', nav)

import 'bootstrap/dist/css/bootstrap.min.css'
import 'jquery/src/jquery.js'
import 'bootstrap/dist/js/bootstrap.min.js'

import "vue-toastification/dist/index.css";

import router from './router'
import store from './store/index'

// import './style.css'
Vue.use(Toast);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
