import Vue from 'vue'
import VueRouter from 'vue-router'
import home from '../components/LandingPage.vue'
import login from '../components/Login.vue'
import dashboard from '../pages/Schedule.vue'
import request from '../pages/request.vue'
import manage from '../pages/manage.vue'
import packages from '../pages/packages.vue'
import clientRequest from '../pages/ClientRequests.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: login,
    alias: '/L'
  },
  {
    path: '/home',
    name: 'home',
    component: home,
    alias: '/H'
  },
  {
    path: '/dashboard', //URL path in the browser
    name: 'Dashboard',//Name
    component: dashboard,
    alias: '/DB'
  },

  {
    path: '/request',
    name: 'Request',
    component: request,
    alias: '/RQ'
  },

  {
    path: '/manage',
    name: 'Manage',
    component: manage,
    alias: '/MG'
  },

  {
    path: '/packages',
    name: 'Package',
    component: packages,
    alias: '/PG'
  },
  {
    path: '/clientRequest',
    name: 'Client Request',
    component: clientRequest,
    alias: '/CR'
  }
]

const router = new VueRouter({
  routes
})

export default router